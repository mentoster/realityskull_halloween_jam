extends RigidBody3D

@export var jump_force: float = 300.0  # сила прыжка


func _physics_process(_delta: float):
    if Input.is_action_just_pressed("jump"):
        apply_central_impulse(Vector3(0, jump_force, 0))
